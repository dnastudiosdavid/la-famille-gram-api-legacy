<?php

namespace App\Http\Controllers;

use App\Models\Score;
use Illuminate\Http\Request;
use App\Contracts\TirettesService;

class APIController extends Controller {

    /**
     * @var TirettesService
     */
    private $tirettesService;

    public function __construct ( TirettesService $tirettesService ) {

        $this->tirettesService = $tirettesService;
    }

    /**
     * Saves an exercise.
     *
     * @param Request $request
     *
     * @param         $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function post_exercise ( Request $request, $code ) {

        $this->tirettesService->storeExercise ( $code, $request->getContent () );
        return response ()->json ( "OK" );
    }

    /**
     * Saves a brand new exercise.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function post_create_exercise ( Request $request ) {

        $exercise = $this->tirettesService->storeNewExercise ( $request->getContent () );
        if ( $exercise != null ) {
            return response ()->json ( $exercise );
        } else {
            return response ()->json ( "Could not create a new exercise.", 404 );
        }
    }

    /**
     * Gets an exercise.
     *
     * @param Request $request
     *
     * @param         $code
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_exercise ( Request $request, $code ) {

        $exercise = $this->tirettesService->getExercise ( $code );
        if ( $exercise == null ) {
            return response ()->json ( "Exercise not found.", 404 );
        } else {
            return response ()->json ( $exercise );
        }
    }

    /**
     * Saves a score.
     *
     * @param Request $request
     *
     * @param         $code
     * @param         $mode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function post_score ( Request $request, $code, $mode ) {

        $scoreEntry = Score::create ( [
            "exercise" => $code,
            "nickname" => $request->get ( "nickname" ),
            "mode" => $mode,
            "score" => intval ( $request->get ( "score" ) )
        ] );
        return response ()->json ( $scoreEntry );
    }

    /**
     * Get scores.
     *
     * @param Request $request
     *
     * @param         $code
     * @param         $mode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_scores ( Request $request, $code, $mode ) {

        return response ()->json ( Score::where ( "exercise", $code )->where ( "mode", $mode )->orderBy ( "score", "desc" )->take ( 10 )->get ( [
            'nickname',
            'score'
        ] ) );
    }
}