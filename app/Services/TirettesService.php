<?php

namespace App\Services;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\Storage;

class TirettesService implements \App\Contracts\TirettesService
{
    /**
     * The application instance.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new filesystem manager instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * @see \App\Contracts\TirettesService::storeNewExercise()
     *
     * @param $raw
     *
     * @return \stdClass
     */
    public function storeNewExercise($raw)
    {
        /** @var \stdClass $exercise */
        $exercise = json_decode($raw);
        $code = null;
        // safe check
        $tries = 200;
        while ($tries >= 0) {
            $tempCode = $this->getCode();
            if (!Storage::disk('local')->exists($tempCode . '.json')) {
                $code = $tempCode;
                break;
            } else {
                $tries--;
            }
        }

        if ($code != null) {
            $exercise->code = $code;
            $this->storeExercise($code, json_encode($exercise));
            return $exercise;
        } else {
            return null;
        }
    }

    /**
     * @see \App\Contracts\TirettesService::storeExercise()
     *
     * @param $code
     * @param $raw
     *
     * @return \stdClass|null
     */
    public function storeExercise($code, $raw)
    {
        Storage::disk('local')->put($code . '.json', $raw);
    }

    /**
     * @see \App\Contracts\TirettesService::getExercise()
     *
     * @param $code
     * @return null|\stdClass
     */
    public function getExercise($code)
    {
        $result = null;
        try {
            $result = json_decode(Storage::disk('local')->get($code . '.json'));
        } catch (FileNotFoundException $e) {
        }
        return $result;
    }

    /**
     * Gets a new randomly generated code for an exercise.
     */
    private function getCode($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }
}
