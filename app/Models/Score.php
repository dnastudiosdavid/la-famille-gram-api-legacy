<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

final class Score extends Model {

    protected $table = "scores";

    protected $fillable = [
        "exercise",
        "nickname",
        "score",
        "mode"
    ];
}