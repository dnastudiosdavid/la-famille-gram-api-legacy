<?php

namespace App\Contracts;

interface TirettesService
{
    /**
     * Stores the exercise given the raw content.
     *
     * @param $code
     * @param $raw
     *
     * @return void
     */
    public function storeExercise($code, $raw);

    /**
     * Stores a new exercise and generates a code.
     *
     * @param $raw
     *
     * @return \stdClass
     */
    public function storeNewExercise($raw);

    /**
     * Gets the exercise given its code.
     *
     * @param $code
     * @return \stdClass|null
     */
    public function getExercise($code);
}
