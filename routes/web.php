<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', 'FrontendController@index');

$app->group([
    'prefix' => 'api/v1/'

], function ($app) {
    // exercises
    $app->post('exercise', 'APIController@post_create_exercise');
    $app->post('exercise/{code}', 'APIController@post_exercise');
    $app->get('exercise/{code}', 'APIController@get_exercise');

    // scores
    $app->post('exercise/{code}/score/{mode}', 'APIController@post_score');
    $app->get('exercise/{code}/score/{mode}', 'APIController@get_scores');
});