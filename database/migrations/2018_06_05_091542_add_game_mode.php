<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGameMode extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up () {

        Schema::table ( 'scores', function ( $table ) {

            $table->string ( 'mode' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down () {

        Schema::table ( 'scores', function ( $table ) {

            $table->dropColumn ( 'mode' );
        } );
    }
}
