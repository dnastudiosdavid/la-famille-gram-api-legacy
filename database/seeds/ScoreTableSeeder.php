<?php

use App\Models\Score;
use Illuminate\Database\Seeder;

class ScoreTableSeeder extends Seeder
{
    public function run()
    {
        Score::create([
            'exercise' => 'test',
            'nickname' => 'David',
            'score' => 215
        ]);

        Score::create([
            'exercise' => 'test',
            'nickname' => 'Roberto',
            'score' => 187
        ]);
    }
}